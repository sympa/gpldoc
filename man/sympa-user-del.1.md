---
title: 'sympa-user-del(1)'
release: '6.2.72'
---

# NAME

sympa-user-del - Delete user

# SYNOPSIS

`sympa user del` _email_

# DESCRIPTION

Deletes a user email address in all Sympa databases (subscriber\_table,
list config, etc) for all virtual robots.
