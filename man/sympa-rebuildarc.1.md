---
title: 'sympa-rebuildarc(1)'
release: '6.2.72'
---

# NAME

sympa-rebuildarc - Rebuild the archives of the list

# SYNOPSIS

`sympa rebuildarc` _list_\[`@`_domain_\]

# DESCRIPTION

Rebuild the archives of the list.
