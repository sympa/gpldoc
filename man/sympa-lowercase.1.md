---
title: 'sympa-lowercase(1)'
release: '6.2.72'
---

# NAME

sympa-lowercase - Lowercase email addresses in database

# SYNOPSIS

`sympa lowercase`

# DESCRIPTION

Lowercase email addresses in database.
