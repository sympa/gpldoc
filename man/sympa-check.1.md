---
title: 'sympa-check(1)'
release: '6.2.72'
---

# NAME

sympa-check - Check environment

# SYNOPSIS

`sympa check`

# DESCRIPTION

Check if `sympa.conf`, `robot.conf` of virtual robots and database structure
are correct.  If any errors occur, exits with non-zero status.
