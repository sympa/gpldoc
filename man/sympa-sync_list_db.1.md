---
title: 'sympa-sync_list_db(1)'
release: '6.2.72'
---

# NAME

sympa-sync\_list\_db - Synchronize database cache of the lists

# SYNOPSIS

`sympa sync_list_db` \[ _list_`@`_domain_ \]

# DESCRIPTION

Syncs filesystem list configs to the database cache of list configs,
optionally syncs an individual list if specified.
