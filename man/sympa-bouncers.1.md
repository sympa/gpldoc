---
title: 'sympa-bouncers(1)'
release: '6.2.72'
---

# NAME

sympa-bouncers - Manipulate list bounced users

# SYNOPSIS

`sympa bouncers` _sub-command_ ...

# DESCRIPTION

TBD.

# SUB-COMMANDS

Currently following sub-commands are available.
To see detail of each sub-command, run '`sympal.pl help bouncers` _sub-command_'.

- ["sympa bouncers del ..."](./sympa-bouncers-del.1.md)

    Unsubscribe bounced users from a list

- ["sympa bouncers reset ..."](./sympa-bouncers-reset.1.md)

    Reset the bounce status of all bounced users of a list

# HISTORY

This option was added on Sympa 6.2.70.
