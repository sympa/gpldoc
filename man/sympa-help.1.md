---
title: 'sympa-help(1)'
release: '6.2.72'
---

# NAME

sympa-help - Display help information about Sympa CLI

# SYNOPSIS

`sympa help` \[ `--format=`_format_ \] \[ _command_... \]

# DESCRIPTION

TBD.
