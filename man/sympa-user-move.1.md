---
title: 'sympa-user-move(1)'
release: '6.2.72'
---

# NAME

sympa-user-move - Change a user email address

# SYNOPSIS

`sympa user move` _current\_email_ _new\_email_

# DESCRIPTION

Changes a user email address in all Sympa  databases (subscriber\_table,
list config, etc) for all virtual robots.
