---
title: 'sympa-version(1)'
release: '6.2.72'
---

# NAME

sympa-version - Print the version number of Sympa

# SYNOPSIS

`sympa version`

# DESCRIPTION
