---
title: 'sympa-copy(1)'
release: '6.2.72'
---

# NAME

sympa-copy - Copy the list

# DESCRIPTION

See ["sympa move"](./sympa-move.1.md).
