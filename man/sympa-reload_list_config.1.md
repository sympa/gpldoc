---
title: 'sympa-reload_list_config(1)'
release: '6.2.72'
---

# NAME

sympa-reload\_list\_config - Recreate config cache of the lists

# SYNOPSIS

`sympa reload_list_config` _list_`@`_domain_&#124;_domain_

# DESCRIPTION
