---
title: 'sympa-show_pending_lists(1)'
release: '6.2.72'
---

# NAME

sympa-show\_pending\_lists - Show pending lists

# SYNOPSIS

`sympa show_pending_lists` _domain_

# DESCRIPTION

Print all pending lists for the robot, with information.
