---
title: 'sympa-bouncers-del(1)'
release: '6.2.72'
---

# NAME

sympa-bouncers-del - Unsubscribe bounced users from a list

# SYNOPSIS

`sympa bouncers del` _list_\[ `@`_domain_ \]

# DESCRIPTION

Unsubscribe bounced users from a list.

# HISTORY

This option was added on Sympa 6.2.70.
