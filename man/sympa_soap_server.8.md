---
title: 'sympa_soap_server(8)'
release: '6.2.72'
---

# NAME

sympa\_soap\_server, sympa\_soap\_server.fcgi - Sympa SOAP server

# DESCRIPTION

Sympa SOAP server allows one to access Sympa service though SOAP.

To know details on Sympa SOAP server, see Sympa Administration Manual:
[https://www.sympa.community/manual/customize/soap-api.html](https://www.sympa.community/manual/customize/soap-api.html).

# HISTORY

Sympa SOAP server appeared on Sympa 4.0.
