---
title: 'sympa-review(1)'
release: '6.2.72'
---

# NAME

sympa-review - Show subscribers of the list.

# SYNOPSIS

`sympa review` \[ `--status` \] _list_\[ `@`_domain_ \]

# DESCRIPTION

Show subscribers of the list.

# HISTORY

This option was added on Sympa 6.2.70.
