---
title: 'sympa-md5_digest(1)'
release: '6.2.72'
---

# NAME

sympa-md5\_digest - Output a MD5 digest

# SYNOPSIS

`sympa md5_digest` _string_

# DESCRIPTION

Output a MD5 digest of a string.
It is useful as password digest for SOAP client trusted application.
