---
title: 'sympa-conf_2_db(1)'
release: '6.2.70'
---

# NAME

sympa-conf\_2\_db - Load config into the database

# SYNOPSIS

`sympa conf_2_db`

# DESCRIPTION

Load sympa.conf and each robot.conf into database.
