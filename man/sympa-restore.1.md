---
title: 'sympa-restore(1)'
release: '6.2.72'
---

# NAME

sympa-restore - Restore users of the lists

# SYNOPSIS

`sympa restore` `--roles=`_role_\[`,`_role_...\] _list_`@`_domain_&#124;`"*"`

# DESCRIPTION

Restore users from files dumped by `--dump_users`.

# HISTORY

This option was added on Sympa 6.2.34.
