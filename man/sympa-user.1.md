---
title: 'sympa-user(1)'
release: '6.2.72'
---

# NAME

sympa-user - Manipulate list users

# SYNOPSIS

`sympa user` _sub-command_ ...

# DESCRIPTION

TBD.

# SUB-COMMANDS

Currently following sub-commands are available.
To see detail of each sub-command, run '`sympal.pl help user` _sub-command_'.

- ["sympa user move ..."](./sympa-user-move.1.md)

    Change a user email address
