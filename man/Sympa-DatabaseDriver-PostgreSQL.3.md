---
title: 'Sympa::DatabaseDriver::PostgreSQL(3)'
release: '6.2.72'
---

# NAME

Sympa::DatabaseDriver::PostgreSQL - Database driver for PostgreSQL

# SEE ALSO

[Sympa::DatabaseDriver](./Sympa-DatabaseDriver.3.md).
