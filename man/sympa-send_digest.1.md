---
title: 'sympa-send_digest(1)'
release: '6.2.72'
---

# NAME

sympa-send\_digest - Send digest

# SYNOPSIS

`sympa send_digest` \[ `--keep-digest` \]

# DESCRIPTION

Send digest right now.
If `--keep-digest` is specified, stocked digest will not be removed.
