---
title: 'Sympa::Request::Handler::del_user(3)'
release: '6.2.72'
---

# NAME

Sympa::Request::Handler::del\_user - delete user

# DESCRIPTION

Deletes an user including subscriptions.

## Attributes

See also ["Attributes" in Sympa::Request](./Sympa-Request.3.md#attributes).

- {email}

    _Mandatory_.
    User email address.

# SEE ALSO

[Sympa::Request::Handler](./Sympa-Request-Handler.3.md).
