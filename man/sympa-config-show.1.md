---
title: 'sympa-config-show(1)'
release: '6.2.72'
---

# NAME

sympa-config-show - Show the content of configuration file

# SYNOPSIS

`sympa config show` \[ `--config=`_/path/to/new/sympa.conf_ \]

# DESCRIPTION

Outputs all configuration parameters in `sympa.conf`.

Options:

- `--config`, `-f=`_/path/to/new/sympa.conf_

    Use an alternative configuration file.

# HISTORY

See [sympa config](./sympa-config.1.md).
