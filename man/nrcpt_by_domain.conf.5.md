---
title: 'nrcpt_by_domain.conf(5)'
release: '6.2.72'
---

# NAME

nrcpt\_by\_domain.conf - Grouping factor for SMTP sessions by recipient domains

# DESCRIPTION

`nrcpt_by_domain.conf` defines limit of the number of recipients for a
particular domain in each session.

TBD.

# FILES

- `$DEFAULTDIR/nrcpt_by_domain.conf`

    Distribution default.  This files should not be edited.

- `$SYSCONFDIR/nrcpt_by_domain.conf`

    Configuration file.

# SEE ALSO

[bulk(8)](./bulk.8.md),
[sympa\_config(5)](./sympa_config.5.md).

# HISTORY

This document was initially written by IKEDA Soji <ikeda@conversion.co.jp>.
